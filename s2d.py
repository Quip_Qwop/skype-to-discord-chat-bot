import discord
import asyncio
import sqlite3
import xml.etree.ElementTree
import sys

client = discord.Client()
conn = sqlite3.connect("main.db")
c = conn.cursor()

def junk_wrap(s):
    return "<junk>" + s + "</junk>"

def get_raw_message(message):
    message = junk_wrap(message)
    return ''.join(xml.etree.ElementTree.fromstring(message).itertext())


@client.event
@asyncio.coroutine
#where the stuff happens
def my_background_task():
    yield from client.wait_until_ready()#wait for the client to be ready
    channel = discord.Object(id=sys.argv[1])#get the channel to use. the id is the first big number in the servers url
    while not client.is_closed: #while the client is open
        otf = open("old_timestamp","r+")#open the file to read
        old_time = otf.read()#read the previous timestamp
        otf.close()#close the file
        otf = open("old_timestamp", "w")#overwrite the file
        if old_time == "":#if the file is empty
            sql = "SELECT timestamp FROM Messages ORDER BY timestamp DESC LIMIT 1"#get the newest time stamp to avoid duplicate messages
            c.execute(sql)
            q = c.fetchone()
            old_time = int(q[0])
        sql = "SELECT timestamp, author, body_xml FROM Messages WHERE timestamp>" + str(old_time) + " ORDER BY timestamp"
        print(sql)
        for row in c.execute(sql):#for each message
            msg = "Author: " + row[1] + "\tMessage: " + get_raw_message(row[2])
            yield from client.send_message(channel,msg)#send the message
        otf.write(str(row[0]))#write the newest timestamp to the file to track which messages to get
        otf.close()#close the file
        yield from asyncio.sleep(5) # task runs every 5 seconds

@client.event
@asyncio.coroutine
def on_ready():# this function runs once
    #prints login info
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')
    #once you see this in the console, you're good to go

#stuff to make the stuff work
loop = asyncio.get_event_loop()

try:
    loop.create_task(my_background_task())
    loop.run_until_complete(client.login(sys.argv[2],sys.argv[3]))
    loop.run_until_complete(client.connect())
except Exception:
    loop.run_until_complete(client.close())
finally:
    loop.close()