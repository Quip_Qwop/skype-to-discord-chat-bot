#About  
This bot is a skype-to-discord chat transfer bot. It analyses the chat history of skype's message database file and processes each row of the Messages table into a human-readable format, and sends it as a message to the provided chat room. It stores the timestamp of the newest message in the file named old_timestamp, and uses it to find which messages haven't been delivered yet. 
#Usage  
clone the repo into the folder with the main.db file. On Windows, its under C:\Users\$NAME\AppData\Roaming\Skype\$SKYPENAME\  

run the command as ```python3 s2d.py $SERVERID $BOTEMAIL $BOTPASSWORD```  

wait for it to connect, and you're good to go
#Requirements
This bot primarily uses Python3, [discord.py](https://github.com/Rapptz/discord.py) and the asyncio library